#!/bin/bash

# create virtual environment
pip install virtualenv
virtualenv venv

# install dependencies
pip install -Uvr requirements.txt

# install Jupyter kernel
ipython kernel install --user --name=venv

jupyter notebook

exit 0
