# job-jumio
Technical assignment for Jumio

## Create the archive
```bash
bash build_archive.bash
```
## Extract the archive
```bash
mkdir archive && tar xzf archive.tar.gz -C archive/ && cd archive
```
## Launch the notebook
```bash
bash run.bash
```
